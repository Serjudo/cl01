package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.service.ProductoOracleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoOracleService productoOracleService;

    @GetMapping("")
    public String index() {
        return "API REST Tech U! v2.0.0";
    }

    //GET Todos los productos (collection)
    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoOracleService.findAll();
    }

    //POST para crear un producto
    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel newProduct) {
        productoOracleService.save(newProduct);
        return newProduct;
        //return new ResponseEntity<String>("Producto creado correctamente", HttpStatus.CREATED);
    }

    //GET producto por ID (instancia)
    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoById(@PathVariable String id) {
        return productoOracleService.findById(id);
    }

    //PUT para actualizar un producto desde el body
    @PutMapping("/productos")
    public void putProducto(@RequestBody ProductoModel productoToUpdate) {
        productoOracleService.save(productoToUpdate);
    }

    //PUT para actualizar un producto por ID
    @PutMapping("/productos/{id}")
    public void putProducto(@PathVariable String id, @RequestBody ProductoModel productoToUpdate) {
        productoOracleService.save(productoToUpdate);

    }

    //DELETE un producto por el body
    @DeleteMapping("/productos")
    public boolean deleteProducto(@RequestBody ProductoModel productoToDelete) {
        return productoOracleService.delete(productoToDelete);
    }

}
