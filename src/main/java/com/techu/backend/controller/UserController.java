package com.techu.backend.controller;

import com.techu.backend.model.UserModel;
import com.techu.backend.service.UserOracleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

@RequestMapping("${url.base}")
public class UserController {
    @Autowired
    UserOracleService userOracleService;

    @GetMapping("")
    public String index() {
        return "API REST Tech U! v2.0.0";
    }

    //GET Todos los usuarios (collection)
    @GetMapping("/usuarios")
    public List<UserModel> getUsers() {
        return userOracleService.findAll();
    }

    //POST para crear un usuario
    @PostMapping("/usuarios")
    public UserModel postUsuario(@RequestBody UserModel newUser) {
        return this.userOracleService.save(newUser);
    }

}
