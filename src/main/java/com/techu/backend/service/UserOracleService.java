package com.techu.backend.service;

import com.techu.backend.model.UserModel;
import com.techu.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserOracleService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        return this.userRepository.findAll();
    }

    public UserModel save(UserModel user) {
        return this.userRepository.save(user);
    }
}
