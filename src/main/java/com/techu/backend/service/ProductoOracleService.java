package com.techu.backend.service;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoOracleService {

    @Autowired
    ProductoRepository productoRepository;

    //READ
    public List<ProductoModel> findAll() {
        return productoRepository.findAll();
    }

    //CREATE
    public ProductoModel save(ProductoModel newProduct) {
        productoRepository.save(newProduct);
        return newProduct;
    }

    //READ BY ID
    public Optional<ProductoModel> findById(String id) {
        return productoRepository.findById(id);
    }

    //DELETE un producto por el body
    public boolean delete(ProductoModel productoModel) {
        try {
            productoRepository.delete(productoModel);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}
