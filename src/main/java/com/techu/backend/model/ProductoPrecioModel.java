package com.techu.backend.model;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCT_PRECIO_TABLE")
public class ProductoPrecioModel {
    @Column(name = "ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    @Column(name = "PRECIO")
    private double precio;

    public ProductoPrecioModel() {}

    public ProductoPrecioModel(String id, double precio) {
        this.id = id;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
