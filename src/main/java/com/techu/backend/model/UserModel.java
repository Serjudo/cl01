package com.techu.backend.model;

import javax.persistence.*;

@Entity
public class UserModel {

    @Column(name = "ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    @Column(name = "USER_NAME", nullable = true, length = 255)
    private String name;

    @Column(name = "USER_ROL", nullable = true, length = 255)
    private String rol;

    public UserModel() {}

    public UserModel(String id, String name, String rol) {
        this.id = id;
        this.name = name;
        this.rol = rol;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
}
