package com.techu.backend.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PRODUCT_TABLE")
public class ProductoModel {

    @NotNull
    @Column(name = "ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    @Column(name = "PRODUCTO_DESCRIPCION", nullable = true, length = 255)
    private String name;

    @Column(name = "PRODUCT_PRECIO", nullable = true, length = 10)
    private Double precio;

    @Column(name = "PRODUCT_USERS", nullable = true)
    private List<UserModel> users;


    public ProductoModel() {}

    public ProductoModel(@NotNull String id, String name, Double precio, List<UserModel> users) {
        this.id = id;
        this.name = name;
        this.precio = precio;
        this.users = users;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }
}
